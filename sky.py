from urllib.request import urlopen
from bs4 import BeautifulSoup
import mysql.connector
from urllib.parse import quote

# Connexion à la base de données
conn = mysql.connector.connect(
    host="localhost",
    user="John",
    password="docu",
    database="crap"
)
cursor = conn.cursor()


def get_page_content(url):
    return urlopen("https://www.paranormaldatabase.com"+url).read()

# get content home (récupérer le contenu de la page)
html_home = get_page_content("/index.html")

# get category home (récupérer la liste des catégories de la page)
list_cat_home = []

soup = BeautifulSoup(html_home, features="html.parser")

liste_thirds = soup.find_all(class_="w3-third")
for third in liste_thirds:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

liste_halfs = soup.find_all(class_="w3-half")
for third in liste_halfs:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

liste_q = soup.find_all(class_="w3-quarter")
for third in liste_q:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

# récupérer la titre de chaque catégories
    
for cat in list_cat_home:
    html_cat = get_page_content(cat)
    soup = BeautifulSoup(html_cat, features="html.parser")
    titre_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
    titre_cat.find("a").extract()
    titre_cat = titre_cat.getText()
    titre_cat = titre_cat.replace("> ", "")
    titre_cat = titre_cat.strip()   

  

    # Insérer les catégories dans la base de données

    if titre_cat.startswith('Reports'):
        cat_type = 'reports'
    else:
        cat_type = 'geography'
        
    sql = "INSERT INTO category (nom, cat_type) VALUES (%s, %s)"
    cursor.execute(sql, (titre_cat, cat_type)) 
    conn.commit()   

# récupérer la liste des sous-catégories
    
    list_sous_cat = []

    liste_thirds = soup.find_all(class_="w3-third")
    for third in liste_thirds:
        lien = third.find('a')
        url= lien.get('href')
        list_sous_cat.append(url)

    liste_halfs = soup.find_all(class_="w3-half")
    for third in liste_halfs:
        lien = third.find('a')
        url= lien.get('href')
        list_sous_cat.append(url)  

# récupérer le titre de chaque sous-catégorie
        
    for ss_cat in list_sous_cat:
        html_sous_cat = get_page_content(ss_cat)
        soup = BeautifulSoup(html_sous_cat, features="html.parser")
        # prendre plutot le titre h3 de la page des cas
        titre_ss_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
        titre_ss_cat.find("a").extract()
        titre_ss_cat.find("a").extract()
        titre_ss_cat = titre_ss_cat.getText()
        titre_ss_cat = titre_ss_cat.replace("> ", "")
        titre_ss_cat = titre_ss_cat.strip()    
       
 # Insérer les catégories dans la base de données       

        cursor.execute("INSERT INTO category (nom, cat_type) VALUES (%s, %s);", (titre_ss_cat, cat_type))
        conn.commit()
       

# récupérer tous les cas
        
        # refaire tout pour chaque page de la pagination
        qs = "?"
        while(qs):
            html_sous_cat = get_page_content(ss_cat+qs)
            soup = BeautifulSoup(html_sous_cat, features="html.parser")
            list_cas = soup.select("div.w3-panel:nth-child(4) > .w3-half > .w3-border-left.w3-border-top.w3-left-align > p")

            for cas in list_cas:
                titre_cas = cas.select_one("h4").get_text()
                p = cas.select_one("p:nth-last-child(1)")
                p_text = p.get_text().split("\n")
                p_text[0] = p_text[0].replace("Location: ", "")
                p_text[1] = p_text[1].replace("Type: ", "")
                p_text[2] = p_text[2].replace("Date / Time: ", "")
                p_text[3] = p_text[3].replace("Further Comments: ", "")
                
                cursor.execute("SELECT id_cas FROM cas WHERE title = %s", (titre_cas,))
                existing_entry = cursor.fetchone()
                if existing_entry:
                    pass
                else:
                    # Insérer uniquement si le titre du cas n'existe pas déjà
                    sql = "INSERT INTO cas (title, location, type, date, comments) VALUES (%s, %s, %s, %s, %s)"
                    val = (titre_cas, p_text[0], p_text[1], p_text[2], p_text[3])
                    cursor.execute(sql, val)
                    conn.commit()
        
                    # Mise à jour du paramètre de requête pour passer à la page suivante
            next_page_link = soup.select_one(".w3-quarter.w3-container h5 a")
            if next_page_link:
                qs = next_page_link.get("href")
            else:
                qs = None  # Aucune page suivante, arrête la boucle

cursor.close()
conn.close()

print(cursor.rowcount, "record inserted.")


