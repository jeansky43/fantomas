-- SELECT 
--     e.element_id,
--     e.title,
--     e.location,
--     e.element_type,
--     e.temporality,
--     e.comments
-- FROM 
--     element e
-- INNER JOIN 
--     element_category ec ON e.element_id = ec.element_id
-- INNER JOIN 
--     category c ON ec.category_id = c.category_id
-- WHERE 
--     c.name = 'Berkshire'
-- LIMIT 25;





-- SELECT 
--     element_id,
--     title,
--     location,
--     element_type,
--     temporality,
--     comments
-- FROM 
--     element
-- WHERE 
--     title LIKE '%John Euerafriad%'
-- LIMIT 5;

-- SELECT 
--     element_id,
--     title,
--     location,
--     element_type,
--     temporality,
--     comments
-- FROM 
--     element
-- WHERE 
--     comments LIKE '%John%';

-- SELECT 
--     element_id,
--     title,
--     location,
--     element_type,
--     temporality,
--     comments
-- FROM 
--     element
-- WHERE 
--     location LIKE '%John%';

-- SELECT 
--     element_id,
--     title,
--     location,
--     element_type,
--     temporality,
--     comments
-- FROM 
--     element
-- WHERE 
--     temporality LIKE '%John%';

SELECT *
FROM element e
INNER JOIN 
    element_category ec ON e.element_id = ec.element_id
INNER JOIN 
    category c ON ec.category_id = c.category_id
WHERE MATCH(name) AGAINST (%s in natural language mode);

SELECT * FROM element
WHERE  MATCH(title) AGAINST ('Lady Grey' in natural language mode);

SELECT * FROM element
WHERE MATCH(location, element_type, temporality, comments) AGAINST (%s IN NATURAL LANGUAGE MODE);

SELECT 
    element_id,
    title,
    location,
    element_type,
    temporality,
    comments
FROM 
    element
WHERE 
    MATCH(location) AGAINST ('John');

SELECT 
    element_id,
    title,
    location,
    element_type,
    temporality,
    comments
FROM 
    element
WHERE 
    MATCH(temporality) AGAINST ('John');

ALTER TABLE element ADD FULLTEXT INDEX idx_title_comments_location_temporality (title, comments, location, temporality);

