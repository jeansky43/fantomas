from datetime import datetime
import mysql.connector

# Connexion à la base de données
conn = mysql.connector.connect(
    host="localhost",
    user="John",
    password="docu",
    database="fantohm"
)
cursor = conn.cursor()

def search(text, cursor):
    t = datetime.now()
    print("\nsearch ", text)
    
    # Requête SQL pour rechercher dans les éléments en fonction des catégories
    req0 = """
        SELECT e.* FROM element e
        INNER JOIN element_category ec ON e.element_id = ec.element_id
        INNER JOIN category c ON ec.category_id = c.category_id
        WHERE MATCH(c.name) AGAINST (%s in natural language mode);
    """
    cursor.execute(req0, (text,))
    result = set(cursor.fetchall())
    print("category ", len(result), "\t\t\t", datetime.now() - t)
    
    # Requête SQL pour rechercher dans les éléments en fonction du titre
    req1 = """
        SELECT * FROM element
        WHERE MATCH(title) AGAINST (%s in natural language mode);
    """
    cursor.execute(req1, (text,))
    result.update(cursor.fetchall())
    print("category + title ", len(result), "\t\t", datetime.now() - t)
    
    # Requête SQL pour rechercher dans les éléments en fonction de plusieurs colonnes
    req2 = """
        SELECT * FROM element
        WHERE MATCH(location, element_type, temporality, comments) AGAINST (%s IN NATURAL LANGUAGE MODE);
    """
    cursor.execute(req2, (text,))
    result.update(cursor.fetchall())
    print("category + title + content ", len(result),'', datetime.now() - t)
    
    return result

search('south east', conn.cursor())
search('Lady Grey', conn.cursor())
search('John Euerafriad', conn.cursor())
search('John', conn.cursor())
search('Man with No Face', conn.cursor())
search('Dark Man', conn.cursor())
search('Pooka', conn.cursor())
search('White Lady', conn.cursor())



